'''
IDvACC Discord Bot
Copyright (C) 2020 Wildan Gunawan <gwildan50@gmail.com>, Frederick Arisandi <frederick.sandi25@gmail.com>

IDvACC Discord Bot is a free software you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
at your option) any later version.

IDvACC Discord Bot is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Aurora Store.  If not, see <http://www.gnu.org/licenses/>.
'''

# Indonesia vACC Bot Discord
# Rewrite Version
# Version Number 2
version = "2"

# Import all required
# libraries to work

import xml.etree.ElementTree as et_xml
import discord, aiohttp, io, pickle, re
import mysql.connector, requests
import asyncio, ssl, urllib.request, json, os, sys
from discord.ext import commands, tasks
from discord.utils import get
from mysql.connector import Error
from itertools import cycle
from datetime import datetime, date
from geopy import distance
from html2text import HTML2Text
from dotenv import load_dotenv
from metar import Metar
from decimal import Decimal
from checkQuarter import CheckQuarter

# day in bahasa Indonesia
hari = {"Mon": "Senin", "Tue": "Selasa", "Wed": "Rabu", "Thu": "Kamis", "Fri": "Jum'at", "Sat": "Sabtu", "Sun": "Minggu"}
bulan = {"Jan":"Januari", "Feb":"Februari", "Mar":"Maret", "Apr":"April", "May":"Mei", "Jun":"Juni", "Jul":"Juli", "Agt":"Agustus", "Sep":"September", "Oct":"Oktober", "Nov":"November", "Dec":"Desember"}

# Bot prefix command
intents = discord.Intents.default()
intents.members = True
bot = commands.Bot(command_prefix='.', intents= intents)

# Bot unique status
statuses = [".help", "idvacc.id", "Euroscope", "X-Plane", "Thor: Ragnarok", "Minecraft"]
status = cycle(statuses)

# Remove default bot command
bot.remove_command('help')

# VARIABEL UNTUK CEK PENDAFTARAN - START
load_dotenv()
dev = True
api_endpoint = os.getenv('api_endpoint')
database_host = os.getenv('database_host')
database_user = os.getenv('database_user')
database_pwd = os.getenv('database_pwd')
database_db = os.getenv('database_db')
if dev:
	bot_token = os.getenv('bot_token_dev')
	guildID = os.getenv('guild_id_dev')
else:
	bot_token = os.getenv('bot_token')
	guildID = os.getenv('guild_id')
# VARIABEL UNTUK CEK PENDAFTARAN - END

# FUNCTION UNTUK CEK PENDAFTARAN - START
def getMember():
	parameter = {
		'limit': 1000
	}
	
	headers = {
		'Content-Type': 'application/json',
		'Authorization': 'Bot ' + bot_token
	}
	
	r = requests.get(api_endpoint + '/guilds/' + guildID + '/members', params=parameter, headers=headers)
  
	return r.json()
# GET MEMBER - END

# remove html tag
def cleanhtml(raw_html):
	cleanr = re.compile('<.*?>|&([a-z0-9]+|#[0-9]{1,6}|#x[0-9a-f]{1,6});')
	cleantext = re.sub(cleanr, '', raw_html)
	return cleantext

# CONVERT SQL RESULT TO LIST - START
def convertDBDiscordtoList(data):
	# Siapkan variabel untuk hasil akhir
	hasilakhir = []
	
	# Iterate over given data
	for data in data:
		# Check which data does not null
		if data[0] != "":
			hasilakhir.append(data[0])
		elif data[1] != "":
			hasilakhir.append(data[0])
	
	# Return result
	return hasilakhir
# CONVERT SQL RESULT TO LIST - END
# FUNCTION UNTUK CEK PENDAFTARAN - END

# Fungsi untuk cek staff atau bukan
def check_staff(sender_roles):
	if dev:
		staff = int(os.getenv('dev_staff'))
	else:
		staff = int(os.getenv('staff'))

	# for loop dalam rolenya
	for x in sender_roles:
	 	# get id
		role_id = x.id
		
		# cek apakah staff atau bukan
		if role_id == staff:
			# staff
			return True
	
	# setelah berulang loop
	# bukan staff juga pfftt
	return False

def check_pilot_role(sender_roles):
	if dev:
		pilot = int(os.getenv('dev_pilot'))
	else:
		pilot = int(os.getenv('pilot'))

	# for loop dalam rolenya
	for x in sender_roles:
	 	# get id
		role_id = x.id
		
		# cek apakah staff atau bukan
		if role_id == pilot:
			# staff
			return True
	
	# setelah berulang loop
	# bukan staff juga pfftt
	return False

def check_idn12(roles):
	for x in roles:
		role_id = x.id
		
		if role_id == int(os.getenv('idn1')) or role_id == int(os.getenv('idn2')):
			return True
	return False

def reqGif():
	tenorApiKey = "VJN8XDXAC8JV"
	limit = 1
	search_term = "slap"
	filter = "medium"

	req = requests.get("https://api.tenor.com/v1/random?q=%s&key=%s&limit=%s&contentfilter=%s" % (search_term, tenorApiKey, limit, filter))
	
	if req.status_code == 200:
		slap_gif = json.loads(req.content)
		url = slap_gif["results"][0]["media"][0]["mediumgif"]["url"]
		return url
	else:
		return None

# Decode ATC Rating
def atcRating(rating):
	if rating == -1:
		return "Inactive"
	elif rating == 0:
		return "Suspended"
	elif rating == 1:
		return "OBS"
	elif rating == 2:
		return "S1"
	elif rating == 3:
		return "S2"
	elif rating == 4:
		return "S3"
	elif rating == 5:
		return "C1"
	elif rating == 7:
		return "C3"
	elif rating == 8:
		return "I1"
	elif rating == 10:
		return "I3"
	elif rating == 11:
		return "SUP"
	elif rating == 12:
		return "ADM"

# Decode Pilot Rating
def pilotRating(rating):
	if rating == 0:
		return "NEW"
	elif rating == 1:
		return "PPL"
	elif rating == 3:
		return "IR"
	elif rating == 7:
		return "CMEL"
	elif rating == 15:
		return "ATPL"
	else:
		return "Unknown"

#==================================== Forloop function ====================================#
async def create_and_check():
	# wait until bot ready
	await bot.wait_until_ready()

	guild = bot.get_guild(int(guildID))

	if dev:
		# [xplane, overwatch]
		voice_room_id = [int(os.getenv('xplane')), int(os.getenv('overwatch'))]
	else:
		# [Aula Nurtanio, Ruang Pilot, Exam/Training Room]
		voice_room_id = [int(os.getenv('aula_nurtanio')), int(os.getenv('ruang_pilot')),
						 int(os.getenv('exam_training'))]

	voice_room = []
	new_voice = []
	for x in voice_room_id:
		tmp = bot.get_channel(x)
		voice_room.append(tmp)

	while not bot.is_closed():
		#remove created channel
		if len(new_voice) > 0:
			for x in new_voice:
				if (len(x.members) == 0):
					await x.delete()
					new_voice.remove(x)

		for x in voice_room:
			if (len(x.members) > 0):
				member = x.members[0]
				category = bot.get_channel(x.category.id)
				new_name = "🟢" + x.name

				#creating new voice channel
				tmp = await category.create_voice_channel(new_name)
				new_voice.append(tmp)

				#member moved
				await member.move_to(tmp)

		await asyncio.sleep(1)

"""
MASIH BELUM BISA PAKAI CUSTOM STATUS, TERNYATA
async def main_status():
	await bot.wait_until_ready()

	custom_activity = discord.CustomActivity(name = "Hit you with that ddu-du ddu-du du")
	await bot.change_presence(activity=custom_activity)
"""

# change bot status in interval
@tasks.loop(minutes=10)
async def change_status():
	# wait until bot ready
	await bot.wait_until_ready()

	# current status
	current_status = next(status)

	# self-explanatory
	if (current_status == "Thor: Ragnarok"):
		# bot is watching to ...
		await bot.change_presence(activity=discord.Activity(type=discord.ActivityType.watching, name=current_status))
	elif (current_status == "Minecraft"):
		# bot is streaming ...
		await bot.change_presence(activity=discord.Streaming(name = "Building stuff on Minecraft!", url = "https://idvacc.id", platform = "Twitch"))
	else:
		# bot is playing ...
		await bot.change_presence(activity=discord.Game(name=current_status, type=0))

# event poster
async def eventautopost():
	# wait until bot ready
	await bot.wait_until_ready()

	# get utc time as the command called
	utc = datetime.utcnow()

	# check if now is 0500z
	if (utc.strftime("%H") == "13"):
		# channel #halaman-event
		channel_to_post = bot.get_channel(int(os.getenv('halaman_event')))

		# get current utc timestamp
		utctimestamp = datetime.utcnow().timestamp()

		# get json data from api
		link = "WIIF.json"
		with open(link) as url:
			# initialize data
			data = json.load(url)

			# for loop over data
		for x in range(len(data)):
			currentdata = data[x]
			eventtime = int(currentdata['datetime'])
			# check if data time is in future and eventtime is 3 days or less to go
			if (utctimestamp < eventtime) and (utctimestamp+60*60*24*3 > eventtime):
				# tarik data
				name = currentdata['name']
				banner = currentdata['bannerLink']
				eventlink = currentdata['link']
				eventtime = datetime.utcfromtimestamp(int(currentdata['datetime']))
				description = currentdata['description'] + "\n\nTo view the detail, please visit " + eventlink
				bannerName = name.replace(" ", "_")

				# parse html tag in description
				h = HTML2Text()
				h.ignore_links = False
				description = h.handle(description)

				# pattern to remove all []
				pattern = r'\[[^\]]*\]'
				description = re.sub(pattern, '', description)

				# init embed
				embed=discord.Embed(title=name, url=eventlink, color=0xffffff)
				embed.set_footer(text="Indonesia vACC. For flight simulation use only.", icon_url="https://idvacc.id/images/logo/id2.png")

				# embed data
				embed.add_field(name="Date", value=eventtime.strftime("%A, %-d %B %Y %H:%M") + "Z")
				embed.add_field(name="Detail", value=description)

				# get image and send to user
				async with aiohttp.ClientSession() as session:
					async with session.get(banner) as resp:
						if resp.status != 200:
							return print("Cannot download image for " + name)
						data = io.BytesIO(await resp.read())
						await channel_to_post.send(file=discord.File(data, bannerName + '.png'))

				# send embed to user
				await channel_to_post.send(embed=embed)

				# all done, sleep for 23.5 hours
				await asyncio.sleep(60*60*23 + 60*30)
	else:
		await asyncio.sleep(5)

# bot check user registered or not
@tasks.loop(hours=12)
async def check_registered():
	# wait until bot ready
	await bot.wait_until_ready()

	# Ambil data di DB
	try:
		# Connect to DB
		mydb = mysql.connector.connect(host=database_host, user=database_user, passwd=database_pwd, database=database_db)

		# Connected to DB
		if mydb.is_connected():
			# Get cursor
			mycursor = mydb.cursor()

			# SQL query
			sql = "SELECT DISTINCT t1.`id-discord`, t2.`cid`, t2.`firstname`, t2.`lastname`, t2.`atcRatingID` FROM `discord` as t1 JOIN `vatsim-account` as t2 ON t1.`cid-vatsim` = t2.`cid` WHERE t1.`id-discord` <> ''"

			# Execute SQL command
			mycursor.execute(sql)

			# Get all result
			sql_result = mycursor.fetchall()

			# Convert result to list only
			data_discord_account = convertDBDiscordtoList(sql_result)

	except Error as e:
		# Error while running command above
		print("Error code 1")
		print("Error while connecting to MySQL", e)

	finally:
		# Finally close DB connection
		if (mydb.is_connected()):
			mycursor.close()
			mydb.close()

	# Remove user role
	try:
		# Get guild member
		members = getMember()

		# Change sql_result name
		data_registered = sql_result

		# Loop si member dan cek satu-satu
		for x in range(len(members)):

			# Ambil user id si member
			user_id = members[x]['user']['id']

			# get my guild
			myguild = bot.get_guild(int(guildID))

			# convert
			user = myguild.get_member(int(user_id))

			# Cek apakah sudah ada di DB atau belum
			if user_id not in data_discord_account:

				# hapus role
				# Cek apakah dia user beneran atau bot
				if user != None and user.bot != True:

					# hapus rolenya dia
					await user.edit(roles=[])

	except:
		# Error while running command above
		print("Error code 2")
		print("Error while PM-ing to user")

# check online atc
# online ATC
callsign_and_name = {}

@tasks.loop(minutes=3)
async def check_online_atc():
	# wait until bot ready
	await bot.wait_until_ready()

	# get guild id
	guild = bot.get_guild(int(guildID))

	# get broadcast room id
	if dev:
		broadcast_room_id = int(os.getenv('dev_broadcast_room'))
	else:
		broadcast_room_id = int(os.getenv('bot_spam'))

	# get room obj
	channel_to_post = bot.get_channel(broadcast_room_id)

	# url to check
	link = "http://api.idvacc.id/atc.php?callsign=WA,WI"

	allowed = ["CTR", "APP", "DEP", "TWR", "GND", "DEL"]
	rejected = ["X", "M"]
	
	# embed supaya rapi katanya
	embed = discord.Embed(title="Ding dong!", description="ATC is comingggg (or leaving???)", color=0xffffff)
	embed.set_footer(text="Indonesia vACC. For flight simulation use only.", icon_url="https://idvacc.id/images/logo/id2.png")

	# current data yg online
	current_data = []

	# list yang online ataupun offline
	online = []
	offline = []

	# string untuk yg online ataupun offline
	kata_kata_online = ""
	kata_kata_offline = ""

	# get data
	with urllib.request.urlopen(link) as url:
		datas = json.loads(url.read().decode())

	for data in datas:
		# ambil sisanya boys
		callsign = data['callsign'].split("_")
		sufix = callsign[-1]
		infix = ""

		if len(callsign) == 3:
			infix = callsign[-2]

		if sufix in allowed and infix not in rejected:
			callsign = data['callsign']
			atc_name = data['name'].title()
			frequency = data['frequency']

			if callsign not in callsign_and_name:
				callsign_and_name[callsign] = atc_name

				# masukkan data ke list online
				online.append([atc_name, callsign, frequency])

			# masukkan ke current data
			current_data.append(callsign)
	
	for callsign in tuple(callsign_and_name):
		# cek apakah ada di callsign_and_name
		if callsign not in current_data:
			# get atc name
			atc_name = callsign_and_name[callsign]

			# masukkan data ke list offline
			offline.append([atc_name, callsign])

			# delete data from dictionary
			del callsign_and_name[callsign]

	# cek apakah ada yang online ataupun offline
	if online != [] or offline != []:
		# siapkan tulisan
		if online != []:
			for x in range(0, len(online)):
				kata_kata_online += f"{x+1}. {online[x][0]} on position {online[x][1]} in frequency {online[x][2]}\n"

			embed.add_field(name="Online", value=kata_kata_online.rstrip())
			
		if offline != []:
			for x in range(0, len(offline)):
				kata_kata_offline += f"{x+1}. {offline[x][0]} offline from position {offline[x][1]}\n"

			embed.add_field(name="Offline", value=kata_kata_offline.rstrip())

		# send
		await channel_to_post.send(embed=embed)

#==========================================================================================#
# Bot login
@bot.event
async def on_ready():
	if dev:
		tmp = bot.get_channel(int(os.getenv('dev_broadcast_room')))
	else:
		tmp = bot.get_channel(int(os.getenv('broadcast_admin')))
	await tmp.send("At your service!")
	print('Logged in! Let\'s start the party!')

# Bot information to help user
@bot.command()
async def botinfo(ctx):
	# bot embed
	embed=discord.Embed(title="Indonesia vACC Discord Bot", url="https://idvacc.id", color=0xffffff)
	embed.set_footer(text="Indonesia vACC. For flight simulation use only.", icon_url="https://idvacc.id/images/logo/id2.png")

	# bot information
	embed.add_field(name="Creator", value="VATSIM Indonesia")
	embed.add_field(name="Website", value="https://idvacc.id")
	embed.add_field(name="Version", value=version)
	embed.add_field(name="Disclaimer", value="This bot and information given by this bot only apply for VATSIM and flight simulation only, not for real flight.")

	# send
	await ctx.send(embed=embed)

@bot.command()
async def trainingreq(ctx, *, content):
	mentor_room_id = int(os.getenv('mentor_room'))
	mentor_tag_id = int(os.getenv('mentor'))
	
	guild = bot.get_guild(int(guildID))
	mentor_role = guild.get_role(mentor_tag_id)
	mention = mentor_role.mention
	mentor_room = bot.get_channel(mentor_room_id)

	#get member
	senderId = ctx.message.author.id
	member = guild.get_member(senderId)

	#message
	text_to_send = mention + "\n```" + member.nick + " Training request:\n" + content + "```"

	embed = discord.Embed(title="Training Request", description="Request sent. If there is any mentor available, he/she will contact you very soon. Please allow us to respond in 3x24 hours.")
	embed.set_footer(text="Indonesia vACC. For flight simulation use only.", icon_url="https://idvacc.id/images/logo/id2.png")
	await ctx.send(embed=embed)
	await mentor_room.send(text_to_send)


# Bot help to help user
@bot.command(aliases=['bantuan', 'man'])
async def help(ctx):
	# get message sender
	sender = ctx.message.author.id

	# bot embed page 1
	embed1 = discord.Embed(title="So, you need a hand? (1/5)", description="Below is the command to help you!", color=0xffffff)
	embed1.set_footer(text="Indonesia vACC. For flight simulation use only.", icon_url="https://idvacc.id/images/logo/id2.png")
	embed1.add_field(name=".atc [ICAO | Indonesia | Jakarta | Bali]", value="Gives requested airport current ATC list.", inline=False)
	embed1.add_field(name=".atcinfo CALLSIGN", value="Gives requested ATC information.", inline=False)
	embed1.add_field(name=".dep / departure ICAO", value="Gives current departing traffic from requested airport.", inline=False)
	embed1.add_field(name=".arr / arrival ICAO", value="Gives current arrival traffic to requested airport.", inline=False)
	embed1.add_field(name=".tfc / traffic ICAO", value="Gives current in/out traffic to requested airport.", inline=False)

	# bot embed page 2
	embed2 = discord.Embed(title="So, you need a hand? (2/5)", description="Below is the command to help you!", color=0xffffff)
	embed2.set_footer(text="Indonesia vACC. For flight simulation use only.", icon_url="https://idvacc.id/images/logo/id2.png")
	embed2.add_field(name=".flightinfo / trafficinfo / tfcinfo CALLSIGN", value="Gives requested flight information.", inline=False)
	embed2.add_field(name=".icao ICAO", value="ICAO checker command!", inline=False)
	embed2.add_field(name=".atcrange", value="Gives maximum visibility range for each ATC position", inline=False)
	embed2.add_field(name=".metar ICAO", value="Gives current requested airport METAR.", inline=False)
	embed2.add_field(name=".taf ICAO", value="Gives current requested airport TAF.", inline=False)

	# bot embed page 3
	embed3 = discord.Embed(title="So, you need a hand? (3/5)", description="Below is the command to help you!", color=0xffffff)
	embed3.set_footer(text="Indonesia vACC. For flight simulation use only.", icon_url="https://idvacc.id/images/logo/id2.png")
	embed3.add_field(name=".trainingreq [date] [zulu time] [message]", value="ATC Training request", inline=False)
	embed3.add_field(name=".zulutime", value="Gives Zulu time", inline=False)
	embed3.add_field(name=".waktuzulu", value="Give Zulu time but in Bahasa :p", inline=False)
	embed3.add_field(name=".lbstokgs", value="Convert lbs to kgs", inline=False)
	embed3.add_field(name=".kgstolbs", value="Convert kgs to lbs", inline=False)
	embed3.add_field(name=".todcalc [speed in knot] [current FL/Alt in ft] [Altitude to reach in ft]", value="Give TOD calculation", inline=False)

	# bot embed page 4
	embed4 = discord.Embed(title="So, you need a hand? (4/5)", description="Below is the command to help you!", color=0xffffff)
	embed4.set_footer(text="Indonesia vACC. For flight simulation use only.", icon_url="https://idvacc.id/images/logo/id2.png")
	embed4.add_field(name=".feettom", value="Convert feet to metre", inline=False)
	embed4.add_field(name=".mtofeet", value="Convert metre to feet", inline=False)
	embed4.add_field(name=".update", value="Ask bot to reassign role and name", inline=False)
	embed4.add_field(name=".mystats", value="Gives yours VATSIM statistics", inline=False)
	embed4.add_field(name=".stats / .whois [CID]", value="Gives the requested CID VATSIM statistics", inline=False)
	embed4.add_field(name=".lastsession [CID]", value="Gives the requested CID last ATC Session", inline=False)
	embed4.add_field(name=".quarterlycheck", value="Gives your quarterly hours for current quarter", inline=False)
	
	# bot embed page 5
	embed5=discord.Embed(title="So, you need a hand? (5/5)", description="Below is the command to help you!", color=0xffffff)
	embed5.set_footer(text="Indonesia vACC. For flight simulation use only.", icon_url="https://idvacc.id/images/logo/id2.png")
	embed5.add_field(name=".web", value="Gives Indonesia vACC Website link", inline=False)
	embed5.add_field(name=".feedback", value="Gives ATC feedback link", inline=False)
	embed5.add_field(name=".botinfo", value="Gives information about this bot", inline=False)
	embed5.add_field(name=".help / bantuan / man", value="Gives this message", inline=False)

	# send to user
	msg = await ctx.send(embed=embed1)
	page = 1

	# add reaction to message
	await msg.add_reaction(emoji='\N{Black Left-Pointing Double Triangle}')
	await msg.add_reaction(emoji='\N{Black Left-Pointing Triangle}')
	await msg.add_reaction(emoji='\N{Black Right-Pointing Triangle}')
	await msg.add_reaction(emoji='\N{Black Right-Pointing Double Triangle}')
	await msg.add_reaction(emoji='\N{Black Square for Stop}')

	# get sent message id
	msgID = msg.id

	# function to know which page to show
	def showpage(page):
		if page == 1:
			return embed1
		elif page == 2:
			return embed2
		elif page == 3:
			return embed3
		elif page == 4:
			return embed4
		elif page == 5:
			return embed5

	# wait user to react
	while True:
		try:
			reaction, user = await bot.wait_for('reaction_add', timeout=60.0)
			if reaction is None:
				await msg.clear_reactions()
			elif (sender == user.id) and (msgID == reaction.message.id):
				if reaction.emoji == '\N{Black Left-Pointing Double Triangle}':
					# double triangle to the left
					# make sure the page is not 1
					if page > 1:
						# change page to 1 and return the page
						page = 1
						await msg.edit(embed=showpage(page))
				elif reaction.emoji == '\N{Black Left-Pointing Triangle}':
					# one triangle to the left
					# make sure the page is not 1
					if page > 1:
						# change page to page - 1
						# edit the message to show
						# help on the requested page
						page -= 1
						await msg.edit(embed=showpage(page))
				elif reaction.emoji == '\N{Black Right-Pointing Triangle}':
					# one triangle to the right
					# make sure the page is not 5
					if page < 5:
						# change page to page + 1 and return the page
						page += 1
						await msg.edit(embed=showpage(page))
				elif reaction.emoji == '\N{Black Right-Pointing Double Triangle}':
					# two triangle to the right
					# make sure the page is not 4
					if page < 5:
						# change page to 4 and return the page
						page = 5
						await msg.edit(embed=showpage(page))
				elif reaction.emoji == '\N{Black Square for Stop}':
					# users want to stop the message/delete it
					await msg.delete()
					break
				if user.id != bot.user.id:
					await reaction.remove(user)
		except asyncio.TimeoutError:
			await msg.clear_reactions()
			break

# flight info
@bot.command(aliases=['tfcinfo', 'trafficinfo'])
async def flightinfo(ctx, args):
	# capitalize input
	args = args.upper()

	link = "http://api.idvacc.id/pilot.php?callsign=" + args
	with urllib.request.urlopen(link) as url:
		# initialize data
		data = json.loads(url.read().decode())
	
	if len(data) != 0:
		# get data
		try:
			callsign = data[0]['callsign']
			pilot = data[0]['name'] + " (" + str(data[0]['cid']) + ")"
			ordes = data[0]["flight_plan"]['departure'] + "-" + data[0]["flight_plan"]['arrival']
			route = data[0]["flight_plan"]['route']
			remarks = data[0]["flight_plan"]['remarks']
			flightmap = "https://vatmap.jsound.org/?c=" + callsign
			fplFilled = True
		except:
			fplFilled = False
		
		# function to get current aircraft status
		def getStatus(dep, arr):
			# open coordinate
			with open("coordinate.pkl", "rb") as cdata:
				try:
					# get the data
					arptcoordinate = pickle.load(cdata)
				
					# get data
					totalDist = (distance.distance(arptcoordinate[dep], arptcoordinate[arr])).nm
					distToArr =  (distance.distance(arptcoordinate[arr], (data[0]['latitude'], data[0]['longitude']))).nm
					distFromDep = (distance.distance(arptcoordinate[dep], (data[0]['latitude'], data[0]['longitude']))).nm
					
					# get data from API
					altitude = int(data['altitude'])
					plannedFL = int(data[0]["flight_plan"]['altitude'])
					groundSpeed = int(data[0]['groundspeed'])
					ifrORvfr = data[0]["flight_plan"]['flight_rules']

					# determine current flight status
					if ifrORvfr == "I":
						# self explanatory
						# if IFR
						if groundSpeed < 5 and altitude < 10000 and distFromDep < 10:
							return "Boarding..."
						elif groundSpeed < 30 and altitude < 10000 and distFromDep < 10:
							return "Taxiing to runway..."
						elif groundSpeed > 30 and altitude < 10000 and distFromDep < 10:
							return "Taking off..."
						elif groundSpeed > 80 and altitude < 10000 and distFromDep > 10 and distFromDep<100:
							return "Initial climb..."
						elif groundSpeed > 80 and altitude > 10000 and (distFromDep/totalDist) < 0.1:
							return "Climbing..."
						elif groundSpeed > 80 and (plannedFL-altitude) < 1000:
							return "Cruising..."
						elif groundSpeed > 80 and altitude < plannedFL and distToArr >= 150 and distToArr < 200:
							return "Descending..."
						elif groundSpeed > 80 and altitude < 10000 and distToArr < 150:
							return "Approaching..."
						elif groundSpeed > 80 and altitude < 10000 and distToArr < 10:
							return "Landing..."
						elif groundSpeed < 30 and altitude < 10000 and distToArr < 10:
							return "Taxiing to apron..."
						elif groundSpeed < 5 and altitude < 10000 and distToArr < 10:
							return "Deboarding..."
						else:
							return "Confused..."
					elif ifrORvfr=="V":
						# if VFR
						if groundSpeed < 5 and altitude < 10000 and distFromDep < 5:
							return "Boarding..."
						elif groundSpeed < 15 and altitude < 10000 and distFromDep < 5:
							return "Taxiing to runway..."
						elif groundSpeed > 15 and altitude < 10000 and distFromDep < 5:
							return "Taking off..."
						elif groundSpeed > 50 and altitude < 10000 and distFromDep > 5 and distFromDep<15:
							return "Initial climb..."
						elif groundSpeed > 50 and altitude > 3000 and (distFromDep/totalDist) < 0.1:
							return "Climbing..."
						elif groundSpeed > 50 and (plannedFL-altitude) < 1000:
							return "Cruising..."
						elif groundSpeed > 50 and altitude < plannedFL and distToArr >= 25 and distToArr < 50:
							return "Descending..."
						elif groundSpeed > 50 and altitude < 10000 and distToArr < 25:
							return "Approaching..."
						elif groundSpeed > 50 and altitude < 10000 and distToArr < 5:
							return "Landing..."
						elif groundSpeed < 15 and altitude < 10000 and distToArr < 5:
							return "Taxiing to apron..."
						elif groundSpeed < 5 and altitude < 10000 and distToArr < 5:
							return "Deboarding..."
						else:
							return "Sightseeing.."
				except:
					return "Cannot get current status..."

		# check if user already filled fpl
		if fplFilled == False:
			await ctx.send("I cannot get their flight plan. Please try again 5 minutes.")
		else:
			# embed supaya rapi
			embed = discord.Embed(title="Flight info of " + callsign, description="Some information for flight number " + callsign + ":", color=0xffffff)
			embed.set_footer(text="Indonesia vACC. For flight simulation use only.", icon_url="https://idvacc.id/images/logo/id2.png")
			embed.add_field(name="Callsign", value=callsign, inline=False)
			embed.add_field(name="Pilot (CID)", value=pilot, inline=False)
			embed.add_field(name="Origin-Destination", value=ordes, inline=False)
			embed.add_field(name="Planned Route", value=route, inline=False)
			embed.add_field(name="Remarks", value=remarks, inline=False)
			embed.add_field(name="Status", value=getStatus(data[0]["flight_plan"]['departure'], data[0]["flight_plan"]['arrival']), inline=False)
			embed.add_field(name="View Online", value=flightmap, inline=False)
			embed.set_footer(text="Indonesia vACC. For flight simulation use only.", icon_url="https://idvacc.id/images/logo/id2.png")
			
			# send data
			await ctx.send(embed=embed)
	else:
		await ctx.send(args + " does not exist in my database.")

# atc info
@bot.command()
async def atcinfo(ctx, args):
	# capitalize input
	args = args.upper()
	args = args.split("_")
	
	if args[1] == "ATIS":
		link = "https://api.idvacc.id/atis.php?callsign=" + "_".join(args)
	else:
		link = "https://api.idvacc.id/atc.php?callsign=" + "_".join(args)
	
	with urllib.request.urlopen(link) as url:
		# initialize data
		data = json.loads(url.read().decode())
	
	if len(data) != 0:
		# get data
		callsign = data[0]['callsign']
		name = data[0]['name'] + " (" + str(data[0]['cid']) + ")"
		rating = atcRating(data[0]['rating'])
		frequency = data[0]['frequency']
		try:
			atis = "\n".join(data[0]['text_atis'])
		except:
			atis = False

		if args[1] == "ATIS":
			atis_code = data[0]['atis_code']
		else:
			atis_code = False

		# embed data
		embed = discord.Embed(title="ATC info for " + callsign, description="Some information for " + callsign + ":", color=0xffffff)
		embed.set_footer(text="Indonesia vACC. For flight simulation use only.", icon_url="https://idvacc.id/images/logo/id2.png")
		embed.add_field(name="Callsign", value=callsign, inline=False)
		embed.add_field(name="Name (CID)", value=name, inline=False)
		embed.add_field(name="Rating", value=rating, inline=False)
		embed.add_field(name="Frequency", value=frequency, inline=False)
		if atis_code != False:
			embed.add_field(name="ATIS Code", value=atis_code, inline=False)
		if atis != False:
			embed.add_field(name="ATIS", value=atis, inline=False)
		# send data
		await ctx.send(embed=embed)
	else:
		await ctx.send(args + " does not exist on my database.")


# online atc
@bot.command()
async def atc(ctx, args):
	# capitalize input
	args = args.upper()

	# reject dan allowed sufix infix
	allowed = ["CTR", "APP", "DEP", "TWR", "GND", "DEL"]
	rejected = ["X", "M"]

	# setup link
	if args == "INDONESIA" or args == "ID" or args == "INDO":
		link = "http://api.idvacc.id/atc.php?callsign=WA,WI"
		args = "Indonesia"
	elif args == "BALI" or args == "DPS" or args == "DENPASAR":
		link = "http://api.idvacc.id/atc.php?callsign=WADD"
		args = "I Gusti Ngurah Rai"
	elif args == "JAKARTA" or args == "JKT":
		link = "http://api.idvacc.id/atc.php?callsign=WIII,WIHH"
		args = "Jakarta"
	elif args == "CENGKARENG" or args == "CGK" or args == "SOETTA":
		link = "http://api.idvacc.id/atc.php?callsign=WIII"
		args = "Soekarno-Hatta Intl"
	else:
		link = "http://api.idvacc.id/atc.php?callsign=" + args

	# get data
	with urllib.request.urlopen(link) as url:
		data = json.loads(url.read().decode())

		# init variable
		atis, deli, gnd, twr, tma, ctr = "", "", "", "", "", ""
		natis, ndeli, ngnd, ntwr, ntma, nctr = 1, 1, 1, 1, 1, 1

		# for loop over data
		for x in data:
			callsign = x['callsign'].split("_")
			sufix = callsign[-1]
			infix = ""

			if len(callsign) == 3:
				infix = callsign[-2]

			if sufix in allowed and infix not in rejected:
				# below are self-explanatory
				if sufix == "ATIS":
					atis = atis + str(natis) + ". " + x['callsign'] + " (" + x['frequency'] + ")\n"
					natis += 1

				elif sufix == "DEL":
					deli = deli + str(ndeli) + ". " + x['callsign'] + " (" + x['frequency'] + ")\n"
					ndeli += 1
					
				elif sufix == "GND":
					gnd = gnd + str(ngnd) + ". " + x['callsign'] + " (" + x['frequency'] + ")\n"
					ngnd += 1
					
				elif sufix == "TWR":
					twr = twr + str(ntwr) + ". " + x['callsign'] + " (" + x['frequency'] + ")\n"
					ntwr += 1
					
				elif (sufix == "APP") or (sufix == "DEP"):
					tma = tma + str(ntma) + ". " + x['callsign'] + " (" + x['frequency'] + ")\n"
					ntma += 1
					
				elif sufix == "CTR":
					ctr = ctr + str(nctr) + ". " + x['callsign'] + " (" + x['frequency'] + ")\n"
					nctr += 1

	# setup embed
	embed=discord.Embed(title="ATC List for " + args, color=0xffffff)
	embed.set_footer(text="Indonesia vACC. For flight simulation use only.", icon_url="https://idvacc.id/images/logo/id2.png")

	# check if atis, gnd, etc is not ""
	if atis != "":
		atis = atis.rstrip()
		embed.add_field(name="ATIS", value=atis, inline=False)
	if deli != "":
		deli = deli.rstrip()
		embed.add_field(name="Delivery", value=deli, inline=False)
	if gnd != "":
		gnd = gnd.rstrip()
		embed.add_field(name="Ground", value=gnd, inline=False)
	if twr != "":
		twr = twr.rstrip()
		embed.add_field(name="Tower", value=twr, inline=False)
	if tma != "":
		tma = tma.rstrip()
		embed.add_field(name="TMA", value=tma, inline=False)
	if ctr != "":
		ctr = ctr.rstrip()
		embed.add_field(name="Center", value=ctr, inline=False)
	
	embed.set_footer(text="Indonesia vACC. For flight simulation use only.", icon_url="https://idvacc.id/images/logo/id2.png")

	# return hasil
	if len(data) <= 0:
		await ctx.send("No ATC available at " + args)
	else:
		await ctx.send(embed=embed)

# departure
@bot.command(aliases=['departure'])
async def dep(ctx, args):
	# capitalize input
	args = args.upper()

	# link to interact with
	link = "http://api.idvacc.id/departure.php?icao=" + args
	
	# Set angka untuk identify data ke berapa
	i = 0
	
	# get data
	with urllib.request.urlopen(link) as url:
		data = json.loads(url.read().decode())
		hasil = ""
		for x in data:
			if i is not len(data) - 1:
				hasil = hasil + str(i + 1) + ". " + x['callsign'] + " (to " + x['flight_plan']['arrival'] + ")\n"
			else:
				hasil = hasil + str(i + 1) + ". " + x['callsign'] + " (to " + x['flight_plan']['arrival'] + ")"
			i = i + 1
	# return hasil
	if len(data) <= 0:
		await ctx.send("0 departure aircraft from " + args)
	elif len(data) < 2:
		await ctx.send("There is " + str(len(data)) + " departure from " + args + "\n" + hasil)
	else:
		await ctx.send("There are " + str(len(data)) + " departures from " + args + "\n" + hasil)

# arrival
@bot.command(aliases=['arrival'])
async def arr(ctx, args):
	# capitalize input
	args = args.upper()

	# link to interact with
	link = "http://api.idvacc.id/arrival.php?icao=" + args
	
	# Set angka untuk identify data ke berapa
	i = 0
	
	# get data
	with urllib.request.urlopen(link) as url:
		data = json.loads(url.read().decode())
		hasil = ""
		for x in data:
			if i is not len(data) - 1:
				hasil = hasil + str(i + 1) + ". " + x['callsign'] + " (from " + x['flight_plan']['departure'] + ")\n"
			else:
				hasil = hasil + str(i + 1) + ". " + x['callsign'] + " (from " + x['flight_plan']['departure'] + ")"
			i = i + 1
	# return hasil
	if len(data) <= 0:
		await ctx.send("0 arrival aircraft to " + args)
	elif len(data) < 2:
		await ctx.send("There is " + str(len(data)) + " arrival to " + args + "\n" + hasil)
	else:
		await ctx.send("There are " + str(len(data)) + " arrival to " + args + "\n" + hasil)

# traffic in/out
@bot.command(aliases=['traffic'])
async def tfc(ctx, args):
	# capitalize input
	args = args.upper()

	# variable to work with
	domestic = ""
	domesticcount = 0
	departure = ""
	departurecount = 0
	arrival = ""
	arrivalcount = 0
	
	# setup link
	if args=="INDONESIA" or args=="ID" or args=="INDO":
		inout = "http://api.idvacc.id/inout.php?icao=WA,WI"
		args = "Indonesia"
		argdiminta = "WA,WI"
	elif args=="BALI" or args=="DPS" or args=="DENPASAR":
		inout = "http://api.idvacc.id/inout.php?icao=WADD"
		args = "I Gusti Ngurah Rai"
		argdiminta = "WADD"
	elif args=="JAKARTA" or args=="JKT":
		inout = "http://api.idvacc.id/inout.php?icao=WIHH,WIII"
		args = "Jakarta"
		argdiminta = "WIHH,WIII"
	elif args=="CENGKARENG" or args=="CGK" or args=="SOETTA":
		inout = "http://api.idvacc.id/inout.php?icao=WIII"
		args = "Soekarno-Hatta Intl"
		argdiminta = "WIII"
	else:
		inout = "http://api.idvacc.id/inout.php?icao=" + args
		argdiminta = args
	
	# split argdiminta by commas
	argdiminta = argdiminta.split(",")
	
	# get actual data
	with urllib.request.urlopen(inout) as url:
		data = json.loads(url.read().decode())
		if len(data) != 0:
			for x in data:
				callsign = x['callsign']
				origin = x['flight_plan']['departure']
				destination = x['flight_plan']['arrival']
				for y in argdiminta:
					panjangdata = len(y)
					if (origin[:panjangdata] == y) and (destination[:panjangdata] == y):
						# domestic
						domestic = domestic + str(domesticcount + 1) + ". " + callsign + " (" + origin + "-" + destination + ")\n"
						domesticcount = domesticcount + 1
					elif (origin[:panjangdata] == y):
						# international departure traffic
						departure = departure + str(departurecount + 1) + ". " + callsign + " (to " + destination + ")\n"
						departurecount = departurecount + 1
					elif (destination[:panjangdata] == y):
						# international arrival traffic
						arrival = arrival + str(arrivalcount + 1) + ". " + callsign + " (from " + origin + ")\n"
						arrivalcount = arrivalcount + 1
	
	# remove all newlines trailing
	domestic = domestic.rstrip()
	departure = departure.rstrip()
	arrival = arrival.rstrip()
	
	# check how many traffic in/out
	if departurecount == 0:
		departure = "0 departure aircraft from " + args
	elif arrivalcount == 0:
		arrival = "0 arrival aircraft to " + args
	
	if domesticcount == 0 and departurecount == 0 and arrivalcount == 0:
		await ctx.send("0 aircraft in/out " + args)
	else:
		totalcount = domesticcount + departurecount + arrivalcount

		# setup embed
		embed=discord.Embed(title="Traffic in/out " + args, description="There is " + str(totalcount) + " traffic in/out " + args, color=0xffffff)
		embed.set_footer(text="Indonesia vACC. For flight simulation use only.", icon_url="https://idvacc.id/images/logo/id2.png")

		# setup field
		# check if there is any domestic flight
		if domesticcount != 0:
			embed.add_field(name="Domestic in " + args, value=domestic, inline=False)
			
		# input all international flight
		embed.add_field(name="Departure from " + args, value=departure, inline=False)
		embed.add_field(name="Arrival to " + args, value=arrival, inline=False)

		# send to user
		await ctx.send(embed=embed)

#================================= From v1 ===================================================================================

# metar
@bot.command()
async def metar(ctx, args):
	args = args.upper()

	url = "http://bot.idvacc.id/metarrequest.php?icao=" + args
	with urllib.request.urlopen(url) as url:
		metar = url.read().decode()

	# open airportname list
	with open("airportname.pkl", "rb") as data:
		try:
			airportname = pickle.load(data)
			# get airport name, may raise error if not found
			airport_name = airportname[args]
		except KeyError:
			airport_name = args
	
	# cek ada sama dengan apa enggak
	if "=" in metar:
		# replace jika ada
		metar = metar.replace("=", "")
	
	# cek ada | yang misahin nggak
	if "|" in metar:
		metar = metar.replace("|", " ")

	# try to decode metar
	try:
		decoded = Metar.Metar(metar)
		decoded = decoded.string().split("\n")
		decoded = "\n".join(decoded[:-1])
	except:
		# ada kesalahan
		decoded = ""

	# embed supaya rapi
	embed = discord.Embed(title="METAR for %s" % airport_name, color=0xffffff)
	embed.add_field(name="Raw", value=metar, inline=False)

	if decoded != "":
		embed.add_field(name="Decoded", value=decoded, inline=False)
	embed.set_footer(text="Indonesia vACC. For flight simulation use only.", icon_url="https://idvacc.id/images/logo/id2.png")
			
	# send data
	await ctx.send(embed=embed)

# taf
@bot.command()
async def taf(ctx, args):
	if (args[:2] == "WA") or (args[:2] == "WI"):
		url = "http://bot.idvacc.id/tafrequest.php?icao=" + args
		with urllib.request.urlopen(url) as url:
			taf = url.read().decode()
		
		# open airportname list
		with open("airportname.pkl", "rb") as data:
			try:
				airportname = pickle.load(data)
				# get airport name, may raise error if not found
				airport_name = airportname[args]
			except KeyError:
				airport_name = args

		# embed supaya rapi
		embed = discord.Embed(title="TAF for %s" % airport_name, color=0xffffff)
		embed.add_field(name="Raw version", value=taf, inline=False)
		embed.set_footer(text="Indonesia vACC. For flight simulation use only.", icon_url="https://idvacc.id/images/logo/id2.png")
				
		# send data
		await ctx.send(embed=embed)
	else:
		await ctx.send("This command only available for Indonesia airport.")

#========================================================================================================================

# atc range info
@bot.command()
async def atcrange(ctx):
	# bot embed
	embed=discord.Embed(title="ATC range list", color=0xffffff)
	embed.set_footer(text="Indonesia vACC. For flight simulation use only.", icon_url="https://idvacc.id/images/logo/id2.png")

	# setup field
	embed.add_field(name="Delivery", value="20nm", inline=False)
	embed.add_field(name="Ground", value="20nm", inline=False)
	embed.add_field(name="Tower", value="50nm", inline=False)
	embed.add_field(name="TMA", value="150nm", inline=False)
	embed.add_field(name="Control", value="600nm", inline=False)

	# send to user
	await ctx.send(embed=embed)

# update role
@bot.command()
async def update(ctx):
	# ambil data
	sender = ctx.message.author
	sender_id = ctx.message.author.id
	premium = ctx.message.author.premium_since

	# cek premium atau bukan
	if premium != None:
		# ternyata premium
		premium = 1
	else:
		# bukan premium
		premium = 0

	# kirim ke server datanya
	link = "http://api.idvacc.id/role.php?sid=%s&premium=%s" % (sender_id, premium)

	# get result
	with urllib.request.urlopen(link) as url:
		data = json.loads(url.read().decode())

		# dapetin data-datanya
		nickname = data['nickname']
		role = data['role']
		roles = []

		# get guild object
		guild = bot.get_guild(int(guildID))

		# get role obj
		for role in role:
			role = guild.get_role(int(role))
			roles.append(role)

		# edit user
		await sender.edit(nick=nickname, roles=roles, reason="User request to update his/her account.")
		await ctx.send("Asiap sudah")

@bot.command()
async def cpt(ctx, icao, rating, candidate):
	# dapatin sender punya role
	msg_sender = ctx.message.author.roles

	# cek apakah staff
	cek = check_staff(msg_sender)

	# dapetin id msgnya
	msg = ctx.message

	if cek:
		# capitalize icao dan rating
		icao = icao.upper()
		rating = rating.upper()
		
		# dapetin channel to postnya
		broadcast_room = bot.get_channel(int(os.getenv('broadcast_room')))

		# cek apakah yang exam center apa bukan
		if icao == "WIIF" or icao == "WAAF":
			nama_center = {
				"WIIF": "Jakarta",
				"WAAF": "Ujung Pandang"
			}

			exam_place_name = nama_center[icao]
		elif rating == "S3":
			exam_place_name = {
				"WADD": "Bali",
				"WIII": "Jakarta",
				"WIMM": "Medan",
				"WAAA": "Makassar"
			}

			try:
				exam_place_name = exam_place_name[icao]
			except:
				exam_place_name = icao
		else:
			# bukan center, kita cari nama
			# bandaranya aja kalo gitu
			with open("airportname.pkl", "rb") as data:
				try:
					# load data
					exam_place_name = pickle.load(data)

					# find name
					exam_place_name = exam_place_name[icao]
				except:
					# given args is not valid
					# set airport name as the icao instead
					exam_place_name = icao

		rating_name = {
			"S2": "Tower",
			"S3": "Approach",
			"C1": "Control",
			"C3": "Control"
		}

		try:
			exam_facility_name = "%s %s" % (exam_place_name, rating_name[rating])
		except:
			exam_facility_name = "%s %s" % (exam_place_name, rating)

		text_to_send = "@everyone,\n\n%s is having his %s CPT now at %s. Fly in/out %s to help him earn %s rating. IFR and VFR welcome. :flag_id:" % (candidate, rating, exam_facility_name, icao, rating)

		# cek apakah ratingnya valid
		if rating in tuple(rating_name):
			# send ke broadcast room
			msg_sent = await broadcast_room.send(text_to_send)

			# add reaction
			#await msg_sent.add_reaction(emoji='\N{Flag of Indonesia}')

			# delete msg user
			await msg.delete()
		else:
			# tidak valid
			# kirim balik
			await ctx.send("Ratingnya dalam S2, S3, C1, dan C3 ya. Bukan pakai TWR, APP, dan CTR.")
	else:
		# delete msg user
		await msg.delete()

@bot.command()
async def congratulate(ctx, rating, candidate):
	# dapatin sender punya role
	msg_sender = ctx.message.author.roles

	# cek apakah staff
	cek = check_staff(msg_sender)

	# dapetin id msgnya
	msg = ctx.message

	if cek:
		# upper ratingnya dulu
		rating = rating.upper()

		# dapetin channel to postnya
		broadcast_room = bot.get_channel(int(os.getenv('broadcast_room')))
		
		# siapin teksnya
		text_to_send = "We congratulate %s for becoming %s rated controller! :flag_id:" % (candidate, rating)

		# kirim ke user
		msg_sent = await broadcast_room.send(text_to_send)

		# add reaction
		#await msg_sent.add_reaction(emoji='\N{Flag of Indonesia}')

		# selesai, hapus msg
		await msg.delete()
	else:
		# spam, hapus msgnya
		await msg.delete()

@bot.command(aliases=['announcement'])
async def announce(ctx, *, content):

	# dapatin sender punya role
	msg_sender = ctx.message.author.roles

	# cek apakah staff
	cek = check_staff(msg_sender)

	# dapetin id msgnya
	msg = ctx.message

	# ambil id sender
	sender = ctx.message.author.id

	if cek:

		# tanya bener apa nggak
		await ctx.send("Apakah kamu mau aku kirim gini?")
		msg_sent = await ctx.send("```%s```" % content)

		# ambil id msg yang baru dikirim
		msg_sent_id = msg_sent.id

		# masukkan reactionnya
		await msg_sent.add_reaction(emoji='\N{White Heavy Check Mark}')
		await msg_sent.add_reaction(emoji='\N{Negative Squared Cross Mark}')

		# bilang ditunggu jawabannya
		await ctx.send("Aku tunggu jawabannya 60 detik dari sekarang ya. CEPET!")

		# wait user to react
		while True:
			try:
				# nunggu 60 detik
				reaction, user = await bot.wait_for('reaction_add', timeout=60.0)

				# kalo gak ada reaksi tetep
				# clear reactionnya
				if reaction is None:
					await msg_sent.clear_reactions()

				elif (sender == user.id) and (msg_sent_id == reaction.message.id):
					if reaction.emoji == '\N{White Heavy Check Mark}':
						# kirim pesan ok
						await ctx.send("OK.")

						# send ke broadcast room
						# dapetin channel to postnya
						broadcast_room = bot.get_channel(int(os.getenv('broadcast_room')))

						await broadcast_room.send(content)

					elif reaction.emoji == '\N{Negative Squared Cross Mark}':
						# kirim pesan ke user
						await ctx.send("Yaudah benerin.")
						break

					# clear reaction karena udah beres
					await msg_sent.clear_reactions()
			
			# kelamaan jadi timeout duluan
			except asyncio.TimeoutError:
				await msg_sent.clear_reactions()
				break
	else:
		await msg.delete()

@bot.command()
async def announceEvent(ctx, event_id):

	# dapatin sender punya role
	msg_sender = ctx.message.author.roles

	# cek apakah staff
	cek = check_staff(msg_sender)

	# dapetin id msgnya
	msg = ctx.message

	# ambil id sender
	sender = ctx.message.author.id

	if cek:

		data = requests.get(f"http://hq.vat-sea.com/api/event/{event_id}")
		data = json.loads(data.content)

		if 'error' not in data:
			event_name = data['title']
			event_banner = data['banner_link']
			event_link = f"https://hq.vat-sea.com/event/{event_id}"

			event_date = datetime.utcfromtimestamp(int(data['start'])).strftime("%-d %B %Y %H:%M") + "Z"
			event_description = ' '.join(cleanhtml(data['description']).split()[:30]) + f"...\n\nStarting at {event_date}\nFind more at {event_link}"

			# tanya bener apa nggak
			msg_sent = await ctx.send(f"Apakah kamu mau aku untuk share event {event_name}?\nAku tunggu jawabannya 60 detik dari sekarang ya. CEPET!")

			# ambil id msg yang baru dikirim
			msg_sent_id = msg_sent.id

			# masukkan reactionnya
			await msg_sent.add_reaction(emoji='\N{White Heavy Check Mark}')
			await msg_sent.add_reaction(emoji='\N{Negative Squared Cross Mark}')

			# wait user to react
			while True:
				try:
					# nunggu 60 detik
					reaction, user = await bot.wait_for('reaction_add', timeout=60.0)

					# kalo gak ada reaksi tetep
					# clear reactionnya
					if reaction is None:
						await msg_sent.clear_reactions()

					elif (sender == user.id) and (msg_sent_id == reaction.message.id):
						if reaction.emoji == '\N{White Heavy Check Mark}':
							# kirim pesan ok
							await ctx.send("Sip deh, wait ya.")

							# siapkan embed eventnya
							embed=discord.Embed(title=event_name, url=event_link, description=event_description)
							
							if event_banner != None:
								embed.set_image(url=event_banner)

							embed.set_footer(text="Indonesia vACC. For flight simulation use only.", icon_url="https://idvacc.id/images/logo/id2.png")

							# send ke broadcast room
							# dapetin channel to postnya
							# get broadcast room id
							if dev:
								broadcast_room_id = int(os.getenv('dev_broadcast_room'))
							else:
								broadcast_room_id = int(os.getenv('broadcast_room'))

							broadcast_room = bot.get_channel(broadcast_room_id)

							await broadcast_room.send(embed=embed)

						elif reaction.emoji == '\N{Negative Squared Cross Mark}':
							# kirim pesan ke user
							await ctx.send("Yaudah benerin.")
							break

						# clear reaction karena udah beres
						await msg_sent.clear_reactions()
				
				# kelamaan jadi timeout duluan
				except asyncio.TimeoutError:
					await msg_sent.clear_reactions()
					break
		else:
			await ctx.send("Gak ketemu eventnya bro.")
	else:
		await msg.delete()

# quaterly check
# to be developed
@bot.command()
async def quarterlycheck(ctx):
	# ambil cid user di database
	sender_id = ctx.message.author.id
	channel = ctx.message.channel
	cid = await reqCidFromDB(channel, sender_id)

	# cek visitor atau resident
	status = None

	datas = requests.get(f"http://hq.vat-sea.com/api/vacc/idn/resident")
	datas = json.loads(datas.content)

	for data in datas:
		if int(data['cid']) == cid:
			status = "resident"
			break
	
	if status == None:
		datas = requests.get(f"http://hq.vat-sea.com/api/vacc/idn/visitor")
		datas = json.loads(datas.content)
		for data in datas:
			if int(data['cid']) == cid:
				status = "visitor"
				break
	
	if status != None:
		# hitung-hitung
		check = CheckQuarter()
		totalan = check.getCurrentQuarterHours(cid)

		textToSend = f"```You have passed the requirement! You have already been online for {totalan} hours this quarter. Keep it up!```"

		if status == "resident":
			if int(totalan.split(":")[0]) < 10:
				totalanPecah = totalan.split(":")
				totalDetik = int(totalanPecah[0]) * 3600 + int(totalanPecah[1]) * 60
				totalKurangDetik = 10 * 3600 - totalDetik
				
				totalKurangMenit = totalKurangDetik / 60
				hour, min = divmod(totalKurangMenit, 60)
				kurangnya = "%02d:%02d" % (hour, min)

				textToSend = f"```You were online for {totalan} hours this quarter. You still need to be online for {kurangnya} hours. がんばってよ!```"
		elif status == "visitor":
			if int(totalan.split(":")[0]) < 3:
				totalanPecah = totalan.split(":")
				totalDetik = int(totalanPecah[0]) * 3600 + int(totalanPecah[1]) * 60
				totalKurangDetik = 3 * 3600 - totalDetik
				
				totalKurangMenit = totalKurangDetik / 60
				hour, min = divmod(totalKurangMenit, 60)
				kurangnya = "%02d:%02d" % (hour, min)

				textToSend = f"```You were online for {totalan} hours this quarter. You still need to be online for {kurangnya} hours. がんばってよ!```"
	else:
		textToSend = "You're not an ATC, lol. Interested to join us? Create a support ticket to Indonesia vACC Staff at https://hq.vat-sea.com/."
	
	# send to user
	await ctx.send(textToSend)

# zulu time now (english)
@bot.command()
async def zulutime(ctx):
	# get utc time as the command called
	utc = datetime.utcnow()

	# user complete variable
	sendback = utc.strftime("%A, %-d %B %Y %H:%M") + "Z"

	# send to user
	await ctx.send(sendback)

# zulu time now (bahasa)
@bot.command()
async def waktuzulu(ctx):
	# get utc time as the command called
	utc = datetime.utcnow()

	# convert english to indo
	day = hari[utc.strftime("%a")]
	month = bulan[utc.strftime("%b")]

	# user complete variable
	sendback = day + ", " + utc.strftime("%-d") + " " + month + utc.strftime(" %Y %H:%M") + "Z"

	# send to user
	await ctx.send(sendback)

# convert lbs to kgs
@bot.command()
async def lbstokgs(ctx, lbs):
	# convert to kgs by multiply lbs by 0.45359237
	kgs = round(int(lbs) * 0.45359237, 2)
	await ctx.send(lbs + "lbs is " + str(kgs) + "kgs")

# convert kgs to lbs
@bot.command()
async def kgstolbs(ctx, kgs):
	# convert to kgs by divide kgs by 0.45359237
	lbs = round(int(kgs) / 0.45359237, 2)
	await ctx.send(kgs + "kgs is " + str(lbs) + "lbs")

# convert ft to m
@bot.command()
async def feettom(ctx, ft):
	# convert to m by multiply ft by 0.3048
	m = round(int(ft) * 0.3048, 2)
	await ctx.send(ft + "ft is " + str(m) + "m")

# convert lbs to kgs
@bot.command()
async def mtofeet(ctx, m):
	# convert to ft by divide m by 0.3048
	ft = round(int(m) / 0.3048, 2)
	await ctx.send(m + "m is " + str(ft) + "ft")

# idvacc web
@bot.command()
async def web(ctx):
	# return website link
	await ctx.send("Visit Indonesia vACC Website: https://idvacc.id")

# feedback
@bot.command()
async def feedback(ctx):
	# return feedback link
	await ctx.send("Send feedback for our ATC at https://hq.vat-sea.com/atc/feedback (Login required).")

# icao checker
@bot.command()
async def icao(ctx, args):
	# capitalize input
	args = args.upper()

	# open airportname list
	with open("airportname.pkl", "rb") as data:
		try:
			airportname = pickle.load(data)
			# get airport name, may raise error if not found
			airportname = airportname[args]
			# send back to user what we get
			await ctx.send("```" + args + " is valid. The airport name is " + airportname + ".```")
		except KeyError:
			# given args is not valid
			# send back to user what we get
			await ctx.send("```It's invalid ICAO! I can not found " + args + " in my airport list.```")

async def reqCidFromDB(channel, sender_id):
	sender_cid = None

	# Ambil data di DB
	try:
		async with channel.typing():
			# Connect to DB
			mydb = mysql.connector.connect(host=database_host, user=database_user, passwd=database_pwd, database=database_db)

			# Connected to DB
			if mydb.is_connected():
				# Get cursor
				mycursor = mydb.cursor()

				# SQL query
				sql = "SELECT `cid-vatsim` FROM `discord` WHERE `id-discord` = %s LIMIT 1" % sender_id

				# Execute SQL command
				mycursor.execute(sql)

				# Get all result
				sql_result = mycursor.fetchall()

				# Convert result to list only
				sender_cid = int(sql_result[0][0])

		# cek cidnya udah dapat apa belum
		if sender_cid != None:
			# udah dapat
			return sender_cid
		else:
			# cid belum dapat, error
			raise Error

	except Error as e:
		# Error while running command above
		print("Error code 1")
		print("Error while connecting to MySQL", e)

		await ctx.send("I cannot get your data from the database.")

	finally:
		# Finally close DB connection
		if (mydb.is_connected()):
			mycursor.close()
			mydb.close()

@bot.command()
async def mystats(ctx):
	sender_id = ctx.message.author.id
	channel = ctx.message.channel
	sender_cid = await reqCidFromDB(channel, sender_id)

	await vatsimStats(ctx, sender_id, sender_cid)

@bot.command(aliases=['whois'])
async def stats(ctx, args):
	sender_id = ctx.message.author.id
	await vatsimStats(ctx, sender_id, urllib.parse.quote(args))

def minutesToFormatString(hours):
	hour = str(hours).split(".")[0]
	decimal = (Decimal(hours)%1) * 60
	return hour + ":" + str(decimal).split(".")[0]

async def vatsimStats(ctx, sender_id, cid):
	# https req
	data = requests.get("https://api.vatsim.net/api/ratings/%s" % (cid))
	data2 = requests.get("https://api.vatsim.net/api/ratings/%s/rating_times/" % (cid))
	if data.status_code == 404 or data2.status_code == 404:
		await ctx.send("CID does not exist!")
	else:
		data = json.loads(data.content)
		data2 = json.loads(data2.content)

	# prepare data
	cid = data['id']
	region = data['region']
	division = data['division']
	subdivision = data['subdivision']
	atc = atcRating(data['rating'])
	pilot = pilotRating(data['pilotrating'])
	join = data['reg_date'].replace("T", ", ")
	susp = data['susp_date']
	atc_hours = data2['atc']
	pilot_hours = data2['pilot']
	s1 = data2['s1']
	s2 = data2['s2']
	s3 = data2['s3']
	c1 = data2['c1']
	c3 = data2['c3']
	i1 = data2['i1']
	i3 = data2['i3']
	sup = data2['sup']
	adm = data2['adm']

	embed = discord.Embed(title=f'{cid}: 1/2')
	embed.set_footer(text="Indonesia vACC. For flight simulation use only.", icon_url="https://idvacc.id/images/logo/id2.png")
	embed.add_field(name="ATC Rating", value=atc)
	embed.add_field(name="Pilot Rating", value=pilot)

	embed.add_field(name="Region", value=region)
	embed.add_field(name="Division", value=division)

	if subdivision != "":
		embed.add_field(name="Sub-Division", value=subdivision)
	else:
		embed.add_field(name="Sub-Division", value="-")

	embed.add_field(name="Joined on", value=join)
	if susp != None:
		embed.add_field(name="Suspended Until", value=susp)

	embed2 = discord.Embed(title=f'{cid}: 2/2')
	embed2.set_footer(text="Indonesia vACC. For flight simulation use only.", icon_url="https://idvacc.id/images/logo/id2.png")
	embed2.add_field(name="Pilot Hours", value=minutesToFormatString(pilot_hours))

	if atc_hours > 0:
		embed2.add_field(name="ATC Hours", value=minutesToFormatString(atc_hours))
	if s1 > 0:
		embed2.add_field(name="S1", value=minutesToFormatString(s1))
	if s2 > 0:
		embed2.add_field(name="S2", value=minutesToFormatString(s2))
	if s3 > 0:
		embed2.add_field(name="S3", value=minutesToFormatString(s3))
	if c1 > 0:
		embed2.add_field(name="C1", value=minutesToFormatString(c1))
	if c3 > 0:
		embed2.add_field(name="C3", value=minutesToFormatString(c3))
	if i1 > 0:
		embed2.add_field(name="I1", value=minutesToFormatString(i1))
	if i3 > 0:
		embed2.add_field(name="I3", value=minutesToFormatString(i3))
	if sup > 0:
		embed2.add_field(name="SUP", value=minutesToFormatString(sup))
	if adm > 0:
		embed2.add_field(name="ADM", value=minutesToFormatString(adm))
	
	page = 1
	msg = await ctx.send(embed=embed)
	msg_id = msg.id
	await msg.add_reaction(emoji='◀️')
	await msg.add_reaction(emoji='▶️')
	await msg.add_reaction(emoji='⏹️')
	
	while True:
		try:
			reaction, user = await bot.wait_for('reaction_add', timeout=60)
			if reaction is None:
				await msg.clear_reactions()
			elif (sender_id == user.id) and (msg_id == reaction.message.id):
				if reaction.emoji == '◀️':
					if page == 1:
						page = 2
						await msg.edit(embed=embed2)
					elif page == 2:
						page = 1
						await msg.edit(embed=embed)
				elif reaction.emoji == '▶️':
					if page == 1:
						page = 2
						await msg.edit(embed=embed2)
					elif page == 2:
						page = 1
						await msg.edit(embed=embed)
				elif reaction.emoji == '⏹️':
					await msg.clear_reactions()
					break

				if user.id != bot.user.id:
					await reaction.remove(user)
			elif (user.id != sender_id) and (msg_id == reaction.message.id) and (user.id != bot.user.id):
				await reaction.remove(user)
		except asyncio.TimeoutError:
			await msg.clear_reactions()
			break

@bot.command()
async def lastsession(ctx, args):
	# https req
	data = requests.get("https://api.vatsim.net/api/ratings/%s/atcsessions/" % (urllib.parse.quote(args)))
	
	if data.status_code == 404:
		await ctx.send("CID does not exist!")
	else:
		data = json.loads(data.content)
	
	data = data['results']

	if data != []:
		data = data[0]
	else:
		data = False

	if data != False:
		# prepare data
		server = data['server']
		start = data['start']
		start_date = datetime.strptime(start, '%Y-%m-%dT%H:%M:%S')
		end = data['end']
		end_date = datetime.strptime(end, '%Y-%m-%dT%H:%M:%S')
		elapsed = end_date - start_date
		callsign = data['callsign']
		tracked = data['aircrafttracked']
		seen = data['aircraftseen']
		amended = data['flightsamended']
		h_init = data['handoffsinitiated']
		h_recv = data['handoffsreceived']
		h_refs = data['handoffsrefused']
		sqwk_ass = data['squawksassigned']
		crs_alt = data['cruisealtsmodified']
		tmp_altd = data['tempaltsmodified']
		srch_pad = data['scratchpadmods']
		
		embed = discord.Embed(title=f'Last ATC Session ({args})')
		embed.add_field(name="Start", value=start.replace("T", ", "))
		embed.add_field(name="End", value=end.replace("T", ", "))
		embed.add_field(name="Elapsed", value=elapsed)
		embed.add_field(name="Server", value=server)
		embed.add_field(name="Callsign", value=callsign)
		embed.add_field(name="Aircraft Tracked", value=tracked)
		embed.add_field(name="Aircraft Seen", value=seen)
		embed.add_field(name="Amended FP", value=amended)
		embed.add_field(name="Handoff Initiated", value=h_init)
		embed.add_field(name="Handoff Received", value=h_recv)
		embed.add_field(name="Handoff Refused", value=h_refs)
		embed.add_field(name="SQWK Assigned", value=sqwk_ass)
		embed.add_field(name="Cruise Alt. Modified", value=crs_alt)
		embed.add_field(name="Temporary Alt. Modified", value=tmp_altd)
		embed.add_field(name="Scracthpad Modified", value=srch_pad)
		embed.set_footer(text="Indonesia vACC. For flight simulation use only.", icon_url="https://idvacc.id/images/logo/id2.png")
		
		await ctx.send(embed=embed)
	else:
		text_to_send = "Belum pernah online ATC kayanya dia. Kalo dia belum punya rating ATC, coba suruh dia gabung dulu dengan kita ini. Caranya gimana? Buka tiket di https://hq.vat-sea.com/ terus kirim ke Indonesia vACC Staff."
		await ctx.send(text_to_send)

@bot.command(name='slap')
async def slap(ctx, arg):
	delay_time = 10
	gif_url = reqGif()
	sender_id = ctx.message.author.id
	ini_bot = False

	guild = bot.get_guild(int(guildID))
	di_slap = guild.get_member(int(''.join(i for i in arg if i.isdigit())))
	
	if di_slap.bot:
		ini_bot = True

	if gif_url == None:
		req_error_msg = await ctx.send(f"Bad argument, {ctx.message.author.mention}! I slap u instead!")
		await asyncio.sleep(delay_time)
		await req_error_msg.delete()
		return
	else:
		for i in range(len(guild.members)):
			# if member is in channel
			if str(guild.members[i].id) == str(''.join(i for i in arg if i.isdigit())):
				bad_arg = False

				# if slap self
				if str(''.join(i for i in arg if i.isdigit())) == str(sender_id):
					desc = f"lol ok, {ctx.message.author.mention}"
				elif ini_bot:
					desc = f"{ctx.message.author.mention}, I slap u instead!"
				else:
					desc = f"{ctx.message.author.mention} slaps {arg}"

				embed = discord.Embed(title="", description=desc, color=0xffffff)
				embed.set_image(url=gif_url)

				await ctx.send(embed=embed)
				break

@bot.command(name='slowmode')
async def slowmode(ctx, arg = 5):
	sender = ctx.message.author.roles
	await ctx.message.delete()
	delay = int(arg)*60

	# cek staff
	staff = check_staff(sender)

	if staff:
		channel = ctx.message.channel
		if channel.slowmode_delay == 0:
			await channel.edit(slowmode_delay=delay)
		else:
			await channel.edit(slowmode_delay=0)

@bot.command()
async def positionhours(ctx, cid, pos=""):
	sender_id = ctx.message.author.id
	channel = ctx.message.channel
 
	if pos == "":
		pos = cid
		cid = str(await reqCidFromDB(channel, sender_id))
		
	# https req
	data = requests.get("https://api.vatsim.net/api/ratings/%s/atcsessions/?format=json" % (urllib.parse.quote(cid)))
	totalminutes = -1

	if data.status_code == 404:
		await ctx.send("CID does not exist!")
	else:
		data = json.loads(data.content)
	
	data = data['results']
	for x in data:
		if x['callsign'] == pos:
			totalminutes = x["total_minutes_on_callsign"]
			break
	
	if totalminutes == -1:
		await ctx.send(f"{cid} never open {pos} position")
	else:
		hour, min = divmod(totalminutes, 60)
		time = "%02d:%02d" % (hour, min)
		embed = discord.Embed(title=pos, description=time)
		embed.set_footer(text="Indonesia vACC. For flight simulation use only.", icon_url="https://idvacc.id/images/logo/id2.png")
		await ctx.send(embed=embed)

@bot.command()
async def kill(ctx):
	sender = ctx.message.author.roles
	await ctx.message.delete()

	# cek staff
	staff = check_staff(sender)

	if staff:
		await ctx.send("Good bye world")
		await bot.close()
		sys.exit()
	else:
		await ctx.send("Don't you dare!!")

@bot.command()
async def restart(ctx):
	sender = ctx.message.author.roles
	await ctx.message.delete()

	# cek staff
	staff = check_staff(sender)

	if staff:
		await ctx.send("Restarting...")
		os.system("clear")
		os.execv(sys.executable, ['python3.8'] + sys.argv)
	else:
		await ctx.send("Don't you dare!!")

@bot.command()
async def todcalc(ctx, speed, currentAlt, toAlt):
	if float(currentAlt) < float(toAlt):
		await ctx.send("🤪 the correct format is ```.todcalc [speed in knot] [current FL/Alt in ft] [Altitude to reach in ft]```")
	else:
		dist = ((float(currentAlt) - float(toAlt)) / 1000) * 3
		vs = float(speed) * 5
		time = (float(currentAlt) - float(toAlt)) / vs
		embed = discord.Embed(title="TOD Calculation")
		embed.add_field(name="Distance to cover", value=f'{str(round(dist, 2))} NM', inline=False)
		embed.add_field(name="Descent rate", value=f'{str(round(vs, 2))} fpm', inline=False)
		embed.add_field(name="Estimated time needed", value=f'{str(round(time, 2))} minutes', inline=False)
		embed.set_footer(text="Indonesia vACC. For flight simulation use only.", icon_url="https://idvacc.id/images/logo/id2.png")
		await ctx.send(embed=embed)

@bot.command()
async def updateroster(ctx):
	sender = ctx.message.author.roles

	# cek staff
	staff = check_staff(sender)

	if staff:
		guild = ctx.guild
		members = await guild.fetch_members(limit=None).flatten()
		# get all resident roster
		roster = requests.get(f"http://hq.vat-sea.com/api/vacc/idn/resident")
		roster = json.loads(roster.content)

		#get all visitor roster
		visitor = requests.get(f"http://hq.vat-sea.com/api/vacc/idn/visitor")
		visitor = json.loads(visitor.content)

		#merge controller list
		roster.extend(visitor)

		# get 'ATC Active' role
		if dev:
			atcActive = guild.get_role(int(os.getenv('dev_atcactive')))
		else:
			atcActive = guild.get_role(int(os.getenv('atcactive')))

		# remove all 'ATC Active' role from non staff member
		for member in atcActive.members:
			if (not check_staff(member.roles)):
				await member.remove_roles(atcActive, reason="Update ATC Roster")

		# add 'ATC Active' role to the appropiate controller
		for controller in roster:
			if (controller['approved_for'] != None):
				for member in members:
					# get member cid from their nickname
					if (member.nick != None and member.bot == False and check_pilot_role(member.roles) and not check_staff(member.roles)):
						cid = member.nick.split("-")[-1].strip()

						if (cid.isnumeric() and int(cid) == controller["cid"]):
							await member.add_roles(atcActive, reason="Update ATC Roster")
		
		await ctx.send("ATC Roster updated!")
	else:
		await ctx.send("Don't you dare!!")
# ======== LOOP ==================================================================================================
# bot loop to change status
#bot.loop.create_task(change_status())
#bot.loop.create_task(check_registered())
#bot.loop.create_task(eventautopost())
#bot.loop.create_task(main_status())

bot.loop.create_task(create_and_check())
change_status.start()
check_online_atc.start()
#check_registered.start()

# run bot
bot.run(bot_token)
