import pickle

# this is an python script
# to extract required data
# to supply IDvACC Bot

# set finished data variable
airportname = {}
coordinate = {}

# open airport file
with open("airport.dat", "r") as airport:
	# read data line by line
	for data in airport.readlines():
		# remove line break
		data = data.replace("\n", "")
		
		# split data
		data = data.split("|")
		
		# get required data
		# Airport name, IATA, ICAO
		# Latitude, longtitude
		icao, name = data[0], data[1]
		airportcoordinate = (data[2], data[3])
			
		# save data into directory
		airportname[icao] = name
		coordinate[icao] = airportcoordinate

with open('airportname.pkl', 'wb') as f:
	pickle.dump(airportname, f)
    
with open('coordinate.pkl', 'wb') as f:
	pickle.dump(coordinate, f)
