import pickle

airlines = {}

with open("ICAO_Airlines.txt", "r") as data:
	for line in data.readlines():
		line = line.replace("\n", "")
		line = line.split("\t")
		airlines[line[0]] = line[2]

with open('airlines.pkl', "wb") as f:
    pickle.dump(airlines, f)