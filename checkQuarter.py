import datetime, requests, json

class CheckQuarter:
	def __init__(self):
		self.resident = requests.get(f"http://hq.vat-sea.com/api/vacc/idn/resident")
		self.resident = json.loads(self.resident.content)
		self.visitor = requests.get(f"http://hq.vat-sea.com/api/vacc/idn/visitor")
		self.visitor = json.loads(self.visitor.content)
		self.passed = []
		self.failed = []
	
	def getStartEndQuarter(self, quarter, year):
		# cek bulan sekarang ada di quarter mana
		if quarter == 1:
			# ambil timestamp, 1 Januari - 31 Maret
			timestamp_start = datetime.datetime.strptime("01/01/%s 00:00:01" % year, "%d/%m/%Y %H:%M:%S").timestamp()
			timestamp_end = datetime.datetime.strptime("31/03/%s 23:59:59" % year, "%d/%m/%Y %H:%M:%S").timestamp()
		elif quarter == 2:
			# ambil timestamp, 1 April - 30 Juni
			timestamp_start = datetime.datetime.strptime("01/04/%s 00:00:01" % year, "%d/%m/%Y %H:%M:%S").timestamp()
			timestamp_end = datetime.datetime.strptime("30/06/%s 23:59:59" % year, "%d/%m/%Y %H:%M:%S").timestamp()
		elif quarter == 3:
			# ambil timestamp, 1 Juli - 30 September
			timestamp_start = datetime.datetime.strptime("01/07/%s 00:00:01" % year, "%d/%m/%Y %H:%M:%S").timestamp()
			timestamp_end = datetime.datetime.strptime("30/09/%s 23:59:59" % year, "%d/%m/%Y %H:%M:%S").timestamp()
		elif quarter == 4:
			# ambil timestamp, 1 Oktober - 31 Desember
			timestamp_start = datetime.datetime.strptime("01/10/%s 00:00:01" % year, "%d/%m/%Y %H:%M:%S").timestamp()
			timestamp_end = datetime.datetime.strptime("31/12/%s 23:59:59" % year, "%d/%m/%Y %H:%M:%S").timestamp()

		return {"start": int(timestamp_start), "end": int(timestamp_end)}

	def getTimeStamp(self, readable_date, date_format = "%Y-%m-%dT%H:%M:%S"):
		if readable_date != None:
			return int(datetime.datetime.strptime(readable_date, date_format).timestamp())
		else:
			return int(datetime.datetime.now().timestamp())

	def getCurrentQuarterHours(self, cid):
		time = self.getCurrentMonthYear()
		quarter = time['quarter']
		year = time['year']
		
		return self.countHours(cid, quarter, year)
	
	def getCurrentMonthYear(self):
		q1 = ['01', '02', '03']
		q2 = ['04', '05', '06']
		q3 = ['07', '08', '09']
		q4 = ['10', '11', '12']
		month = datetime.datetime.now().strftime("%m")
		year = datetime.datetime.now().strftime("%Y")
		
		if month in q1:
			quarter = 1
		elif month in q2:
			quarter = 2
		elif month in q3:
			quarter = 3
		elif month in q4:
			quarter = 4
		
		return ({"quarter": quarter, "year": year})
	
	def checkMemberCurrentQuarter(self):
		time = self.getCurrentMonthYear()
		quarter = time['quarter']
		year = time['year']
		
		return self.checkMember(quarter, year)
	
	def countHours(self, cid, quarter, year):
		time_slot = self.getStartEndQuarter(quarter, year)
		quarter_start = time_slot['start']
		quarter_end = time_slot['end']
		data = requests.get(f"https://api.vatsim.net/api/ratings/{cid}/atcsessions/")
		data = json.loads(data.content)
		allowed = ["CTR", "APP", "DEP", "TWR", "GND", "DEL"]
		rejected = ["X", "M"]
		total_minutes = 0
		
		loop = True
		while loop:
			next = data['next']
			current_page = data['results']
			live_session = 0
			
			for session in current_page:
				start = self.getTimeStamp(session['start'])
				end = self.getTimeStamp(session['end'])
				
				callsign = session['callsign'].split("_")
				sufix = callsign[-1]
				infix = ""
				
				if len(callsign) == 3:
					infix = callsign[1]
	
				if (sufix in allowed) and (infix not in rejected) and (callsign[0].startswith('WI') or callsign[0].startswith('WA')):
					if quarter_start <= start and end < quarter_end:
						total_minutes += float(session['minutes_on_callsign'])
					elif start <= quarter_start and quarter_start < end:
						total_minutes = total_minutes + (end - quarter_start)
			
			if next != None:
				data = requests.get(next)
				data = json.loads(data.content)
			else:
				break
		
		hour, min = divmod(total_minutes,60)
		time = "%02d:%02d" % (hour, min)
		return time

	def checkMember(self, selected_quarter, selected_year):
		passed = []
		failed = []
		
		# RESIDENT
		print("Checking resident...")
		for member in self.resident:
			cid = member['cid']
			fullname = member['name']
			hours = self.countHours(cid, selected_quarter, selected_year)
			
			if int(hours.split(":")[0]) >= 10:
				passed.append(f"{fullname} ({cid}) ---> {hours}")
			else:
				failed.append(f"{fullname} ({cid}) ---> {hours}")
		# VISITOR
		print("Checking visitor...")
		for member in self.visitor:
			cid = member['cid']
			fullname = member['name']
			hours = self.countHours(cid, selected_quarter, selected_year)
			
			if int(hours.split(":")[0]) >= 3:
				passed.append(f"(VISITOR) {fullname} ({cid}) ---> {hours}")
			else:
				failed.append(f"(VISITOR) {fullname} ({cid}) ---> {hours}")
		
		self.passed = passed
		self.failed = failed
		print("Checking completed!")
		self.createLog(selected_quarter, selected_year)
		#return ({"passed": passed, "failed": failed})
	
	def createLog(self, quarter, year):
		with open(f"ATC Q{quarter} {year} Hours report.txt", "w+") as f:
			f.write("=================== PASSED ===================\n")
			for line in self.passed:
				f.write(line + "\n")
			
			f.write("\n\n=================== FAILED ===================\n")
			for line in self.failed:
				f.write(line + "\n")
		
		print("Log created!")


# FOR LOCAL ONLY
#check = CheckQuarter()
#check.checkMember(4, 2020)
#print(check.countHours(1433164, 2, 2020))
#check.checkMemberCurrentQuarter()
